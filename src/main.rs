mod handlers {
    pub mod main_handler;
}
mod routes {
    pub mod main_route;
}


use actix_web::{App, HttpServer};
use routes::main_route::init_routes;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().configure(init_routes))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}
