use actix_web::{HttpRequest, HttpResponse, Responder};

pub async fn index(_req: HttpRequest) -> impl Responder {
    HttpResponse::Ok().body("Hello, rust_gate!")
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, App, web};

    #[actix_rt::test]
    async fn test_index() {
        let mut app = test::init_service(App::new().route("/", web::get().to(index))).await;
        let req = test::TestRequest::get().uri("/").to_request();
        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), 200);
    }
}
