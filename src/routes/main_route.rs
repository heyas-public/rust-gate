use actix_web::web;
use crate::handlers::main_handler;

pub fn init_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("/").to(main_handler::index));
}
